import re
from Vendedor import Vendedor
from Comision import Comision

class ReadCSV():
    def __init__(self):
        self.archivo =  open("datos.csv","r")
        
    def leer(self):
        fila = 0
        listaRegistros = [] 
        patron= re.compile(",")
        for linea in self.archivo:
            if (fila == 0):
                fila += 1
                continue
            grupos = patron.split(linea)
            listaRegistros.append([Vendedor(grupos[3].rstrip(),grupos[2]),grupos[1],
            float(grupos[0]),Comision(grupos[1],float(grupos[0])).calcularComision()])
        return listaRegistros