class Vendedor():
    def __init__(self, idVendedor, nombreVendedor):
        self.idVendedor = idVendedor
        self.nombreVendedor = nombreVendedor
        
    def getIdVendedor(self):
        return self.idVendedor
    
    def setComision(self, comision):
        self.comisionVendedor = comision
        
    def getNombreVendedor(self):
        return self.nombreVendedor
    
    def __str__(self):
        return "{} {} tiene como comision {}".format(self.getIdVendedor(),self.getNombreVendedor(),self.comisionVendedor)