from DataAbstractionLayer import DataAbstractionLayer
from ReadCSV import ReadCSV

class ListaVentas():
    def leerArray(self):
        lector = ReadCSV()
        self.listaVentas = lector.leer()
        for registro in self.listaVentas:
            print(registro[3])
    
    def totalizar(self):
        datos = DataAbstractionLayer()
        datos.llenarTabla(self.listaVentas)
        self.listaVendedores=datos.calculaComision()
        
    def exportarArray(self):
        for registro in self.listaVendedores:
            print(str(registro))    
            