import sqlite3
from Vendedor import Vendedor

class DataAbstractionLayer():
    def __init__(self):
        self.con = sqlite3.connect("comision.db")
        self.consulta = self.con.cursor()
        cadena = """ CREATE TABLE IF NOT EXISTS comision
            (id_comision INTEGER PRIMARY KEY, id_vendedor CHAR(32) NOT NULL,
            nombre_vendedor CHAR(60), comision REAL);
            """
        self.consulta.execute(cadena)
        self.con.commit()
        
    def llenarTabla(self, lista):
        cadena = "DELETE FROM comision"
        self.consulta.execute(cadena)
        for registro in lista:
            cadena = 'INSERT INTO comision (id_vendedor, nombre_vendedor,comision) VALUES ("{}","{}",{})'.format(registro[0].getIdVendedor(),registro[0].getNombreVendedor(),registro[3])
            self.consulta.execute(cadena)
        self.con.commit()
         
    def calculaComision(self):
        comisiones = []
        cadena = "SELECT SUM(comision),id_vendedor,nombre_vendedor FROM comision GROUP BY id_vendedor ORDER BY id_vendedor;"
        self.consulta.execute(cadena)
        resultado = self.consulta.fetchall()
        for item in resultado:
            nuevoVendedor = Vendedor(item[1],item[2])
            nuevoVendedor.setComision(float(item[0]))
            comisiones.append(nuevoVendedor)
        self.con.close()
        return comisiones
                
    